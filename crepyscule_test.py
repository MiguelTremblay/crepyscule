#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2005,2008  Miguel Tremblay

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not see  <http://www.gnu.org/licenses/>.
############################################################################

"""
Package to test the different function of the crepyscule packcage.

Project home page is U{http://ptaff.ca/crepyscule/}

 - Name:        crepyscule_test.py
 - Author:      U{Miguel Tremblay<http://ptaff.ca/miguel/>}
 - Date:        February 18th 2008       
"""

import crepyscule

import time

# Dates (2008, 6, 18), (2008, 3, 9), (2008, 11, 2)
lCtime = [1213815927, 1205079414, 1225642614]
fUTC = 5.0
lSummerTime = ["", "America/Toronto"]
fUTC = -5.0

# Places [(montreal)]
lLatLon = [(45.4667, -73.75)]
fLat2 = 40.75
fLon2 = -73.99

for sSummerTime in lSummerTime:
    for ctime in lCtime:
        print(("Check for time", time.localtime(ctime)))
        for (fLat, fLon) in lLatLon:
            print("----")
            print("Sunrise/sunset")
            print("get_sunrise_day(%d, %0.2f, %0.2f, %0.2f, %s)" %\
                  (ctime, fLat, fLon, fUTC, sSummerTime))
            fSunrise = crepyscule.get_sunrise_day(ctime, fLat, fLon, \
                                                  fUTC, sSummerTime)
            if fSunrise != None:
                print("OK", fSunrise)
            print("get_sunset_day(%d, %0.2f, %0.2f, %0.2f, %s)" %\
                  (ctime, fLat, fLon, fUTC, sSummerTime))
            fSunset = crepyscule.get_sunset_day(ctime, fLat, fLon, \
                                                fUTC, sSummerTime)
            if fSunset != None:
                print("OK", fSunset)
                print("get_sunrise_as_dict(%d, %0.2f, %0.2f, %0.2f, %s)" %\
                      (ctime, fLat, fLon, fUTC, sSummerTime))
            dSunrise = crepyscule.get_sunrise_as_dict(ctime, fLat, fLon, \
                                                      fUTC, sSummerTime)
            if dSunrise != None:
                print("OK", list(dSunrise.values())[0])
            print("get_sunset_as_dict(%d, %0.2f, %0.2f, %0.2f, %s)" %\
                  (ctime, fLat, fLon,fUTC, sSummerTime))
            dSunset = crepyscule.get_sunset_as_dict(ctime, fLat, fLon, \
                                                    fUTC, sSummerTime)
            if dSunset is not None:
                print("OK", list(dSunset.values())[0])
            print("get_sunrise_sunset_as_csv(%d, %0.2f, %0.2f, %0.2f, %s)" %\
                  (ctime, fLat, fLon, fUTC, sSummerTime))
            lSunriseSunset = crepyscule.get_sunrise_sunset_as_csv\
                             (ctime, fLat, fLon, \
                              fUTC, sSummerTime)
            if lSunriseSunset is not None:
                print("OK")
                print(lSunriseSunset[0])
                print(lSunriseSunset[1])
            print("get_sunrise_sunset_as_csv(%d, %0.2f, %0.2f, %0.2f, %s , %0.2f, %0.2f)" %\
                  (ctime, fLat, fLon, fUTC, sSummerTime, fLat2, fLon2))
            lSunriseSunset2 = crepyscule.get_sunrise_sunset_as_csv\
                             (ctime, fLat, fLon, \
                              fUTC, sSummerTime, fLat2, fLon2)
            if lSunriseSunset2 is not None:
                print("OK")
                print(lSunriseSunset2[0])
                print(lSunriseSunset2[1])

            # Check only the first time. No use for summer time code.
            if sSummerTime != "":
                break
            print("----")
            print("Sun altitude")
            print("get_sun_altitude_day(%d, %0.2f)" % (ctime, fLat))
            fAltitude = crepyscule.get_sun_altitude_day(ctime, fLat)
            if fAltitude is not None:
                print("OK", fAltitude)
            print("get_sun_altitude_as_dict(%d, %0.2f)" % (ctime, fLat))
            dAltitude = crepyscule.get_sun_altitude_as_dict(ctime, fLat)
            if dAltitude is not None:
                print("OK", list(dAltitude.values())[0])
            print("get_sun_altitude_as_csv(%d, %0.2f)" % (ctime, fLat))
            lSunAltitude = crepyscule.get_sun_altitude_as_csv(ctime, fLat)
            if lSunAltitude is not None:
                print("OK")
                print(lSunAltitude[0])
                print(lSunAltitude[1])
            print("get_sun_altitude_as_csv(%d, %0.2f, %0.2f)" % \
                  (ctime, fLat, fLat2))
            lSunAltitude2 = crepyscule.get_sun_altitude_as_csv(ctime,\
                                                               fLat, fLat2)
            if lSunAltitude2 is not None:
                print("OK")
                print(lSunAltitude2[0])
                print(lSunAltitude2[1])
            print("----")
            print("Daylight variation")
            print("get_daylight_variation_day(%d, %0.2f)" % (ctime, fLat))
            fVariation = crepyscule.get_daylight_variation_day(ctime, fLat)
            if fVariation is not None:
                print("OK", fVariation)
            print("get_daylight_variation_as_dict(%d, %0.2f)" % (ctime, fLat))
            dVariation = crepyscule.get_daylight_variation_as_dict(ctime, fLat)
            if dVariation is not None:
                print("OK", list(dVariation.values())[0])
            print("get_daylight_variation_as_csv(%d, %0.2f)" % (ctime, fLat))
            lDaylightVariation = crepyscule.\
                                 get_daylight_variation_as_csv(ctime, fLat)
            if lDaylightVariation is not None:
                print("OK")
                print(lDaylightVariation[0])
                print(lDaylightVariation[1])
            print("get_daylight_variation_as_csv(%d, %0.2f, %0.2f)" %\
                  (ctime, fLat, fLat2))
            lDaylightVariation2 = crepyscule.\
                                  get_daylight_variation_as_csv(ctime,\
                                                                fLat, fLat2)
            if lDaylightVariation2 is not None:
                print("OK")
                print(lDaylightVariation2[0])
                print(lDaylightVariation2[1])
            print("----")
            print("Solar flux")
            print("get_max_sf_day(%d, %0.2f)" % (ctime, fLat))
            fSF = crepyscule.get_max_sf_day(ctime, fLat)
            if fSF is not None:
                print("OK", fSF)
            print("get_max_sf_as_dict(%d, %0.2f)" % (ctime, fLat))
            dSF = crepyscule.get_max_sf_as_dict(ctime, fLat)
            if dSF is not None:
                print("OK", list(dSF.values())[0])
            print("get_max_sf_as_csv(%d, %0.2f)" % (ctime, fLat))
            lSF = crepyscule.get_max_sf_as_csv(ctime, fLat)
            if lSF is not None:
                print("OK")
                print(lSF[0])
                print(lSF[1])
            print("get_max_sf_as_csv(%d, %0.2f, %0.2f)" % (ctime, fLat, fLat2))
            lSF2 = crepyscule.get_max_sf_as_csv(ctime, fLat, fLat2)
            if lSF2 is not None:
                print("OK")
                print(lSF2[0])
                print(lSF2[1])

[English](README.md)

Crepyscule
=============


Introduction
------------

`crepyscule` est un logiciel python3 utilisé pour calculer le lever/coucher du soleil de n’importe quel endroit du monde. Il est utilisé pour les données de la page web [Lever, coucher, durée du jour sur ptaff.ca](http://ptaff.ca/soleil/?lang=fr_CA). Ce projet a été inspiré par Henrik Härkönen et peut être trouvé sur [cette page Web](https://bitbucket.org/heharkon/sun.py?fbclid=IwAR2hthKy549T483tYeP9yBjhREK_hKXJuB50t9cbORT2x4xGK5OMNKNA4fg).



For 1 or 2 geographical location on the earth, crepyscule produces four graphics and or a text file:
1. One graphic showing the time of sunrise, sunset and the total time of a day (the total time being defined, in this case, as the time elapsed between the sunset and the sunrise) for every day of the year;
2. One graphic showing the maximum altitude of the sun for every day of the year;
3. One graphic showing the daily variation of daylight;
4. One graphic showing the solar flux for every day of the year.
5. One file containing the sunset/sunrise values.

If one chooses to show 2 locations, the curves of these 2 locations will lie on the same graphic, using different colors.

Requirements
------------

* [Python3](https://www.python.org/downloads/)
* [Python :: numpy](http://www.numpy.org/)
* [Python :: rpy2](https://rpy2.bitbucket.io/)
* [Python :: pytz](https://pypi.python.org/pypi/pytz)  
* [Cairo](https://www.cairographics.org/)
* [R](https://www.r-project.org/)
* [R :: Cairo](https://cran.r-project.org/web/packages/Cairo/index.html)


Download
--------
The latest version can be downloaded here:<br>
https://framagit.org/MiguelTremblay/crepyscule   

The git version can be accessed here:<br>
 ```git clone https://framagit.org/MiguelTremblay/crepyscule.git```


Manual
--------

In a general way, this software should be called in command line like this:
```bash
python3 crepyscule.py --filename=filename --ctime=ctime --background-color=bg_color --lat1=latitude1 --lon1=longitude1 --utc1=utc1 --tz1=timezone1 [--lat2=latitude2 --lon2=longitude2 --utc2=utc2 --tz2=timezone2] [--graph-sunrise-sunset] [--graph-sun-altitude] [--graph-daylight-variation] [--graph-solar-flux] [--text-sunrise-sunset] [--graph-all]
```

* The parameters between [] are optional; you may compare two locations this way. Note that you should give at least one of the following options: --graph-sunrise-sunset, --graph-sun-altitude, --graph-daylight-variation, --graph-solar-flux, --text-sunrise-sunset ou --graph-all.;
* The language of title in the graphics are determined by the LANGUAGE environment variable.


<br />
where:

| Parameters                                  | Description |
| -------                                  | ------------|
| `-h`, `--help`                           | Show help message and exit.|
| `-V` `--version`                         | Output version information and exit.|
| `-f` `--filename` FILENAME               | The filename parameter is used to indicate the path and the name of the image. The .png extension is mandatory. The first graphic takes this exact name of the parameter; the altitude graphic will be have the same name but with the characters "_alt" added before the extension name .png, the daily variation of daylight graphic will be have the same name but with the characters "_delta_t" added before the extension name .png and the solar flux graphic will be have the same name but with the characters "_sf" added before the extension name .png |
| `--ctime` FTIME                          | The ctime parameter is used to determine for which year the values will be computed. Moreover, when only one location is showed, a dashed vertical line is drawn at the day corresponding to this date. The values at the intersections of this line and the differents curves, for each of the 4 graphics, are written.; |
| `--background-color` COLO          R     | Background color of the graphics. Default value is 'white'. The background-color parameter should be in the [X11 color list](https://en.wikipedia.org/wiki/X11_color_names) and should be written using lower case.|
| `--lat1`&nbsp;lat1                       | Latitude of the first location (negative value for South).  Real number. For example, 45°30' should be written as '45.5'.|
| `--lon1`&nbsp;lon1                       | Longitude of the first location (negative value for West). Real number. For example, 45°30' should be written as '45.5'.|
| `--utc1`&nbsp;utc1                       | Difference of the time zone compared to the UTC time for the first location. For example, for New York this parameter is -5.0.|
| `--tz1`&nbsp;tz1                         | Time zone code for the first location. For example, value for Montréal is "America/Montreal". Possible values are in [TZ database](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones#List)|
| `--lat2`&nbsp;lat2                       | Latitude of the second location (negative value for South). Real number. For example, 45°30' should be written as '45.5'.|
| `--lon2`&nbsp;lon2                       | Longitude of the second location (negative value for West).Real number. For example, 45°30' should be written as '45.5'.|
| `--utc2`&nbsp;utc2                       | Difference of the time zone compared to the UTC time for the second location. For example, for New York this parameter is -5.0|
| `--tz2`&nbsp;tz2                         | Time zone code for the second location. For example, value for Montréal is "America/Montreal". Possible values are in [TZ database](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones#List)|
| `--graph-sunrise-sunset`                 | 	Create the graphic for sunrise/sunset
| `--graph-sun-altitude`                   |	Create the graphic for sun altitude
| `--graph-daylight-variation`             |	Create the graphic for daylight variation
| `--graph-solar-flux`                     |	Create the graphic for the solar flux
| `--graph-twilight-length`                |	Create the graphic for the twilight length
| `--text-sunrise-sunset`                  |	Create a text file containing the sunrise/sunset value
| `--graph-all`                            |	Create all the graphics for these lat/lon



Usage
------------

Create the graphics for the city of Montreal for march 29th 2005:
```bash
python crepyscule.py --graph-all --filename montreal.png --ctime 1112121748 \
--background-color white --lat1 45.4667 --lon1 -73.75 --utc1 -5.0 --tz1 America/Montreal
```

Create one text file with the sunrise/sunset for the city of Montreal for march 29th 2005:
```bash
 python crepyscule.py --text-sunrise-sunset --filename montreal.csv --ctime 1112121748 \
 --background-color white --lat1 45.4667 --lon1 -73.75 --utc1 -5.0 --tz1 America/Montreal
```

Create one text file with sunrise/sunset for the South Pole in 2019
```bash
 python crepyscule.py --text-sunrise-sunset --filename fichier.csv --ctime 1553385600 \
 --lat1 -90 --lon1 0 --utc1 0 --tz1 Etc/GMT
```

API
-----
You can consult [crepyscule's API](https://ptaff.ca/crepyscule/api/).

Bugs
-----

For any bug report, please contact [soleil.miguel@ptaff.ca](mailto:soleil.miguel@ptaff.ca)

Author
-----

[Miguel Tremblay](http://ptaff.ca/miguel/?lang=en_CA)

License
-----

Copyright © 2019 Miguel Tremblay.

get_canadian_weather_observations is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.



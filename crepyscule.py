#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright  2005,2008,2016  Miguel Tremblay

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not see  <http://www.gnu.org/licenses/>.
############################################################################

"""
Package to get different information related to the sun
going around the Earth. It is based on one or two locations
on the Earth, based on the lat/lon and, optional, the different
time zone and summer time codes. 
Currently the following information is given by
this code: sunrise, sunset, sun altitude, daylight variation,
solar flux and twilight.

crepyscule could be use in two modes: for graphics and as a module.
 1. The U{python module numpy<http://numpy.scipy.org/>} is required.
 2. The graphic mode required other modules and is more tedious to
    install. U{rpy<http://rpy.sourceforge.net/>} for python and U{Cairo in R<https://cran.r-project.org/web/packages/Cairo/index.html>} are required.
 3. Starting from version 1.0.0, it is possible to use crepyscule
    as a module, one only needs to uncompress
    crepyscule and could import it and use it directly. It only
    uses standard python package and numpy.

Project home page is U{http://ptaff.ca/crepyscule/}

Note:
  - The file /usr/share/apps/kstars/Cities.dat from software kdeedu
    was used as a base for informations about time zones.  The list of
    available time zone can be found in crepyscule_summer_time.py

 - Name:        crepyscule.py
 - Author:      U{Miguel Tremblay<http://ptaff.ca/miguel/>}
 - Date:        February 12th 2008       
"""

import os
import sys
if 'LANGUAGE' not in os.environ:
   os.environ.setdefault("LANGUAGE","en")
elif "en" not in os.environ["LANGUAGE"] and "fr" not in os.environ["LANGUAGE"] :
   os.environ["LANGUAGE"] = "en"

import crepyscule_graphic
import crepyscule_tools

# Set the place where to fetch languages dependant messages
import gettext
t = gettext.translation('crepyscule', sys.path[0] + '/locale')
_ = t.gettext



VERSION = "1.2.0"


def crepyscule(dDataRequest, fLat1, fLon1, ctime, \
               fUTC1=0, sFilename='',  \
               sSummerTime1='', fLat2= None, \
               fLon2=None, sSummerTime2='', fUTC2=0, \
               sBackgroundColor='white'):
   """
   This is the main function for using crepyscule in command line.
   The result is png images and/or text files containing the requested
   information about the Sun.

   @type   dDataRequest: dictionnary
   @param  dDataRequest: Contains what file(s) or image(s)
    should be created. See the command line options. Keys are
    options with '--' and values are booleans.
    Keys are: 'GRAPH-SUNRISE','GRAPH-ALTITUDE'
    'GRAPH-DAYLIGHT_VARIATION','GRAPH-SOLAR_FLUX', 'GRAPH-TWILIGHT_LENGTH',
    'TEXT-SUNRISE' 
   @type  fLat1: float
   @param fLat1: latitude of the first place. Latitude is in decimal
      degrees. Example: 30°30'  should be 30.5
   @type  fLon1: float
   @param fLon1: longitude of the first place. Longitude is in decimal
      degrees. Example: 30°30'  should be 30.5. West longitude are negative
      East longitude are positive.
   @type ctime: float
   @param ctime: Any ctime in the day for the wanted value. If a graphic is
    created and there is only one place, a line is drawn with the values
    written at the intersection for this specific day.
   @type fUTC1:  float
   @param fUTC1: Time to add/substract for each time values for the first place.
   Usually used for UTC value of place.
   @type  sFilename: string
   @param sFilename: Filename for the images or text. Filename must ends
    with 'png' extension. The sunrise/sunset image is written with this
    filename. This terminology is used::
     sunrise/sunset: filename.png
     altitude: filename_alt.png 
     daylight variation: filename_delta_t.png
     solar flux: filename_sf.png
     sunrise/sunset text file: filename.txt
   @type  sSummerTime1: string
   @param sSummerTime1: This variable is a 2 letters code indicating
    when is the daylight saving time (DST) starting and ending in the year
    for the first place.
    Default is no DST. For a list of DST code,
    see L{crepyscule_summer_time.lTimezone}.
   @type  fLat2: float
   @param fLat2: latitude of the second place. See fLat1.
   @type  fLon2: float
   @param fLon2: longitude of the second place. See fLon1.
   @type  sSummerTime2: string
   @param sSummerTime2: Olson DB code indicating
    when is the daylight saving time (DST) starting and ending in the year
    for the second place.
    Default is no DST. For a list of DST code,
    see L{crepyscule_summer_time.lTimezone}.
   @type fUTC2:  float
   @param fUTC2: Time to add/substract for each time values for the second place.
   @type  sBackgroundColor: string
   @param sBackgroundColor: Color that will be used for the background
    color for the generated graphics. See U{Chart of R colors<http://research.stowers-institute.org/efg/R/Color/Chart/>}
   """

   # If no filename given, use lat and lon
   if sFilename == '' or sFilename is None:
      print ("No file name provided, using sdtout to print the result")
#      sFilename = 'Lat' +str(fLat1) + '_Lon' + str(fLon1) + '.txt'
#      print ("Filename not provided, using: ", sFilename)

   (lSunrise, lSunset, lSunAltitude, tToday, fDaylengthDecember31stLastYear) = \
              crepyscule_tools.get_one_year_data(fLat1, fLon1, \
                                                 ctime, fUTC1, \
                                                 sSummerTime1)
   
   lDaylength = crepyscule_tools.get_daylength(ctime, fLat1, fLon1)
   lDateISO8601 = crepyscule_tools.get_one_year_of_iso8601(ctime)

   # Check if there is a 2nd place to compute lat and lon
   if fLon2 != None and fLat2 != None:
      (lSunrise2, lSunset2, lSunAltitude2, tTmp, fDaylengthDecember31stLastYear2) = \
                  crepyscule_tools.get_one_year_data(fLat2, fLon2, \
                                                     ctime, fUTC2, sSummerTime2)
      lDaylength2 =  crepyscule_tools.get_daylength(ctime, fLat2, fLon2)
   else:
      lSunrise2 = lSunset2 = lSunAltitude2 = lDaylength2 \
          = fDaylengthDecember31stLastYear2 = None

   # Graphics
   if dDataRequest["GRAPH-SUNRISE"]:
      crepyscule_graphic.plot_sunset_sunrise(lSunrise, lSunset, lDaylength, \
                                             lDateISO8601,\
                                             sFilename,  tToday, \
                                             sBackgroundColor, lSunrise2,\
                                             lSunset2, lDaylength2)
   if dDataRequest["GRAPH-ALTITUDE"]:
      crepyscule_graphic.plot_altitude(tToday, lSunAltitude,lDateISO8601,\
                                       sFilename, sBackgroundColor, \
                                       lSunAltitude2)
   if dDataRequest["GRAPH-DAYLIGHT_VARIATION"]:
      crepyscule_graphic.plot_daylight_differences(lSunrise, lSunset,\
                                                   lDaylength,\
                                                   lDateISO8601,\
                                                   sFilename, \
                                                   tToday, sBackgroundColor, \
                                                   fDaylengthDecember31stLastYear, \
                                                   lSunrise2, lSunset2, \
                                                   lDaylength2, \
                                                   fDaylengthDecember31stLastYear2)
   if dDataRequest["GRAPH-SOLAR_FLUX"]:
      crepyscule_graphic.plot_max_solar_flux(lDateISO8601, sFilename, tToday,\
                                      sBackgroundColor, fLat1, fLat2)

   if dDataRequest["GRAPH-TWILIGHT_LENGTH"]:
      dTwilightLength1 = get_twilight_length_as_dict(ctime, fLat1)
      if fLat2 is not None:
         dTwilightLength2 = get_twilight_length_as_dict(ctime, fLat2)
      else:
         dTwilightLength2 = None
      crepyscule_graphic.plot_twilight_length(sFilename, tToday, sBackgroundColor, \
                                           fLat1, dTwilightLength1,\
                                           fLat2, dTwilightLength2)

   if dDataRequest["TEXT-SUNRISE"]:
      crepyscule_tools.save_info_flat_file(sFilename, lDateISO8601,\
                                               lSunrise, lSunset, lDaylength,\
                                               fLat1, fLon1, fUTC1, sSummerTime1, \
                                               lSunrise2, lSunset2, lDaylength2, \
                                               fLat2, fLon2, fUTC2, sSummerTime2)

                                             
##############################################################################
# Sunrise/sunset

def get_sunrise_day(ctime, fLat, fLon, fUTC=0, sSummerTime=''):
   """
   Get the sunrise value for this day.

   @type  fLat: float
   @param fLat: Latitude in decimal.
   @type  fLon: float
   @param fLon: Longitude in decimal. West longitude are negative
      East longitude are positive.
   @type ctime: float
   @param ctime: Any ctime in the day for the wanted value.
   @type fUTC: float
   @param fUTC: Time to add/substract for each time values. Usually used
    for UTC value of place.
   @type  sSummerTime: string
   @param sSummerTime: Olson code indicating
    when is the daylight saving time (DST) starting and ending in the year
    for the first place.
    Default is no DST. For a list of DST code, see L{crepyscule_summer_time.lTimezone}.

   @rtype: float
   @return: Time of the day in float.
   """
   fSunrise = crepyscule_tools.get_one_value("sunrise", fLat, fLon,\
                                             ctime, fUTC, sSummerTime)
   
   return fSunrise

def get_sunset_day(ctime, fLat, fLon, fUTC=0, sSummerTime=''):
   """
   Get the sunset value for this day.

   @type  fLat: float
   @param fLat: Latitude in decimal.
   @type  fLon: float
   @param fLon: Longitude in decimal. West longitude are negative
      East longitude are positive.
   @type ctime: float
   @param ctime: Any ctime in the day for the wanted value.
   @type fUTC: float
   @param fUTC: Time to add/substract for each time values. Usually used
    for UTC value of place.
   @type  sSummerTime: string
   @param sSummerTime:  Olson DB code indicating
    when is the daylight saving time (DST) starting and ending in the year
    for the first place.
    Default is no DST. For a list of DST code, see L{crepyscule_summer_time.lTimezone}.

   @rtype: float
   @return: Time of the day in float.
   """
   fSunset = crepyscule_tools.get_one_value("sunset", fLat, fLon,\
                                             ctime, fUTC, sSummerTime)
   
   return fSunset
   
def get_sunrise_as_dict(ctime, fLat, fLon, fUTC=0, sSummerTime=''):
   """
   Get a dictionary containing the sunrise time for the year of the ctime.
   Keys of dictionary are date in iso8601 format.

   @type  fLat: float
   @param fLat: Latitude in decimal.
   @type  fLon: float
   @param fLon: Longitude in decimal. West longitude are negative
      East longitude are positive.
   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type fUTC: float
   @param fUTC: Time to add/substract for each time values. Usually used
    for UTC value of place.
   @type sSummerTime: string
   @param sSummerTime:  Olson DB code indicating
    when is the daylight saving time (DST) starting and ending in the year
    for the first place.
    Default is no DST. For a list of DST code, see L{crepyscule_summer_time.lTimezone}.

   @rtype: dictionnary
   @return: Dictionnary containing sunrise time for the year of the ctime.
    Keys of dictionary are date in iso8601 format.
   """

   dSunrise = crepyscule_tools.get_dictionary( "sunrise", fLat, fLon, ctime,\
                                              fUTC, sSummerTime)

   return dSunrise

def get_sunset_as_dict(ctime, fLat, fLon, fUTC=0, sSummerTime=''):
   """
   Return a list containing the sunset time for this ctime.

   @type  fLat: float
   @param fLat: Latitude in decimal.
   @type  fLon: float
   @param fLon: Longitude in decimal. West longitude are negative
      East longitude are positive.
   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type fUTC: float
   @param fUTC: Time to add/substract for each time values. Usually used
    for UTC value of place.
   @type sSummerTime: string
   @param sSummerTime:  Olson DB code indicating
    when is the daylight saving time (DST) starting and ending in the year
    for the first place.
    Default is no DST. For a list of DST code, see L{crepyscule_summer_time.lTimezone}.

   @rtype: dictionnary
   @return: Dictionnary containing sunset time for the year of the ctime.
    Keys of dictionary is a string in iso8601 format like 'YYYY-MM-DD'.
   """
   dSunset = crepyscule_tools.get_dictionary("sunset", fLat, fLon, ctime, \
                                             fUTC, sSummerTime)

   return dSunset
   

def get_sunrise_sunset_as_csv(ctime, fLat1, fLon1, fUTC1=0, sSummerTime1='', \
                              fLat2= None, fLon2=None,  fUTC2=0, sSummerTime2=''):
   """
   Returns a list containing the date,sunrise,sunset in a CSV format.
   See function L{crepyscule_tools.get_sunrise_sunset_as_csv}  for
   detailed information.

   @type  fLat1: float
   @param fLat1: Latitude in decimal for the first place.
   @type  fLon1: float
   @param fLon1: Longitude in decimal for the first place.
    West longitude are negative.
    East longitude are positive.
   @type ctime: float
   @param ctime:  Any ctime in the year of the result.
   @type fUTC1:  float
   @param fUTC1: Time to add/substract for each time values for the first place.
   Usually used for UTC value of place.
   @type  sSummerTime1: string
   @param sSummerTime1:  Olson DB code indicating
    when is the daylight saving time (DST) starting and ending in the year
    for the first place.
    Default is no DST. For a list of DST code, see L{crepyscule_summer_time.lTimezone}.
   @type  fLat2: float
   @param fLat2: latitude of the second place. See fLat1.
   @type  fLon2: float
   @param fLon2: longitude of the second place. See fLon1.
   @type  sSummerTime2: string
   @param sSummerTime2:  Olson DB code indicating
    when is the daylight saving time (DST) starting and ending in the year
    for the seconde place.
    Default is no DST. For a list of DST code, see L{crepyscule_summer_time.lTimezone}.
   @type fUTC2:  float
   @param fUTC2: Time to add/substract for each time values for the second place.

   @rtype: list
   @return: list containing the date,sunrise,sunset in CSV format. One line per
    item in the list.
   """
   
   (lSunrise1, lSunset1, lSunAltitude, tToday, fDummy) = \
               crepyscule_tools.get_one_year_data(fLat1, fLon1, \
                                                  ctime, fUTC1, sSummerTime1)
   lDaylength1 = crepyscule_tools.get_daylength(ctime, fLat1, fLon1)
   # Check if there is a 2nd place to compute lat and lon
   if fLon2 != None and fLat2 != None:
      (lSunrise2, lSunset2, lSunAltitude2, tTmp, fDummy) = \
                  crepyscule_tools.get_one_year_data\
                  (fLat2, fLon2, ctime, fUTC2, sSummerTime2)
      lDaylength2 = crepyscule_tools.get_daylength(ctime, fLat2, fLon2)
   else:
      lSunrise2 = lSunset2 = lDaylength2 = None
        
   lDateISO8601 = crepyscule_tools.get_one_year_of_iso8601(ctime)
   lSunriseSunset = crepyscule_tools.\
                    get_sunrise_sunset_as_csv(lDateISO8601,\
                                              lSunrise1, lSunset1, lDaylength1,\
                                              fLat1, fLon1, fUTC1, sSummerTime1,\
                                              lSunrise2, lSunset2, lDaylength2,\
                                              fLat2, fLon2, fUTC2,\
                                              sSummerTime2)

   return lSunriseSunset


                                             
##############################################################################
# Sun altitude
def get_sun_altitude_as_csv(ctime, fLat1, fLat2=None):
   """
   Returns a list containing the date,sun_altitude

   No use for longitude, UTC or summer time. Use default values.
   
   See function L{crepyscule.get_sun_altitude_as_csv} for
   detailed information.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat1: float
   @param fLat1: Latitude in decimal for the first place.
   @type  fLat2: float
   @param fLat2: latitude in decimal for the second place.

   @rtype: list
   @return: list containing the date,sun_altitude in CSV format. One line per
    item in the list.
   
   """
   # Use dummy value since it is the same for all lon, crepyscule_summer_time and utc
   sSummerTime1 = sSummerTime2 = ''
   fLon1 = fLon2 = 0.0
   fUTC1 = fUTC2 = 0
   
   (lSunrise, lSunset, lSunAltitude1, tToday, fDummy) = \
              crepyscule_tools.get_one_year_data(fLat1, fLon1, ctime, fUTC1, \
                                                 sSummerTime1)

   # Check if there is a 2nd place to compute lat and lon
   if fLat2 != None:
      (lSunrise2, lSunset2, lSunAltitude2, tTmp, fDummy) = \
       crepyscule_tools.get_one_year_data\
       (fLat2, fLon2, ctime, fUTC2, sSummerTime2)
   else:
      lSunAltitude2 = None
        

   lDateISO8601 = crepyscule_tools.get_one_year_of_iso8601(ctime)
   lSunAltitude = crepyscule_tools.\
                  get_sun_altitude_as_csv(lDateISO8601, fLat1, lSunAltitude1, \
                                           fLat2, lSunAltitude2)
   
   return lSunAltitude


def get_sun_altitude_as_dict(ctime, fLat):
   """
   Return a list containing the sun altitude for this ctime.

   No use for longitude, UTC or summer time. Use default values.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat: float
   @param fLat: Latitude in decimal for the place.

   @rtype: dictionnary
   @return: Dictionnary containing sun altitude for the year of the ctime.
     Keys of dictionary is a string in iso8601 format like 'YYYY-MM-DD'.
   """
   # Use dummy value since it is the same for all lon, crepyscule_summer_time and utc
   sSummerTime = ""
   fLon = 0.0
   fUTC = 0.0
   
   dSunAltitude = crepyscule_tools.get_dictionary("altitude", fLat, fLon,\
                                                  ctime, fUTC, sSummerTime)

   return dSunAltitude

def get_sun_altitude_day(ctime, fLat):
   """
   Get the sun altitude value for this day.
   No use for longitude, UTC or summer time. Use default values.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat: float
   @param fLat: Latitude in decimal for the place.

   @rtype: float
   @return: Sun altitude for this day in float.
   """
   # Use dummy value since it is the same for all lon, crepyscule_summer_time and utc
   sSummerTime = ""
   fLon = 0.0
   fUTC = 0.0
   
   fAltitude = crepyscule_tools.get_one_value("altitude", fLat, fLon,\
                                             ctime, fUTC, sSummerTime)
   
   return fAltitude

##############################################################################
# Daylight variation


def get_daylight_variation_as_csv(ctime, fLat1, fLat2=None):
   """
   Returns a list containing the date, variation in daylight.

   See function L{crepyscule_tools.get_daylight_variation_as_csv} for
   detailed information.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat1: float
   @param fLat1: Latitude in decimal for the first place.
   @type  fLat2: float
   @param fLat2: latitude in decimal for the second place.

   @rtype: list
   @return: list containing the date,variation in daylight in CSV format.
    One line per item in the list.
   """
   # Use dummy value since it is the same for all lon,
   #  crepyscule_summer_time and utc
   sSummerTime1 = sSummerTime2 = ''
   fLon1 = fLon2 = 0.0
   fUTC1 = fUTC2 = 0.0
   
   lSunTime1 = crepyscule_tools.get_daylength(ctime, fLat1, fLon1)   
   (lSunrise, lSunset, lSunAltitude, tToday, fDaylengthDecember31stLastYear) = \
              crepyscule_tools.get_one_year_data(fLat1, fLon1, \
                                                 ctime, fUTC1, \
                                                 sSummerTime1)

   # Check if there is a 2nd place to compute lat and lon
   if fLon2 != None and fLat2 != None:
      lSunTime2 =  crepyscule_tools.get_daylength(ctime, fLat2, fLon2)  
      (lSunrise2, lSunset2, lSunAltitude2, tToday2, fDaylengthDecember31stLastYear2) = \
              crepyscule_tools.get_one_year_data(fLat2, fLon2, \
                                                 ctime, fUTC2, \
                                                 sSummerTime2)

   else:
      lSunTime2 = fDaylengthDecember31stLastYear2 = None

   lDateISO8601 = crepyscule_tools.get_one_year_of_iso8601(ctime)
   lDaylightVariation = crepyscule_tools.\
                        get_daylight_variation_as_csv(lDateISO8601,\
                                                      lSunTime1, fLat1, fDaylengthDecember31stLastYear, \
                                                      lSunTime2, fLat2, fDaylengthDecember31stLastYear2)

   return lDaylightVariation

def get_daylight_variation_as_dict(ctime, fLat):
   """
   Return a list containing the daylight variation for the year
   correspoding to this ctime in a dictionnary.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat: float
   @param fLat: Latitude in decimal for the place.

   @rtype: dictionnary
   @return: Dictionnary containing daylight variation for the year of the ctime.
    Keys of dictionary is a string in iso8601 format like 'YYYY-MM-DD'.
   """
   # Use dummy value since it is the same for all lon,
   #  crepyscule_summer_time and utc
   sSummerTime = ""
   fLon = 0.0
   fUTC = 0.0
   
   dDayligthVariation = crepyscule_tools.get_dictionary("variation", \
                                                        fLat, fLon, ctime, \
                                                        fUTC, sSummerTime )

   return dDayligthVariation


def get_daylight_variation_day(ctime, fLat):
   """
   Get the sun dayligth variation value for this day.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat: float
   @param fLat: Latitude in decimal for the place.

   @rtype: float
   @return: Daylight variation for this day in float.
   """
   # Use dummy value since it is the same for all lon,
   #  crepyscule_summer_time and utc
   sSummerTime = ""
   fLon = 0.0
   fUTC = 0.0
   
   fVariation = crepyscule_tools.get_one_value("variation", fLat, fLon,\
                                             ctime, fUTC, sSummerTime)
   
   return fVariation

##############################################################################
# Solar flux

def get_max_sf_as_csv(ctime, fLat1, fLat2=None):
   """
   Returns a list containing the date, maximum solar flux

   See function L{crepyscule_tools.get_solar_flux_as_csv} for
   detailed information.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat1: float
   @param fLat1: Latitude in decimal for the first place.
   @type  fLat2: float
   @param fLat2: Latitude in decimal for the second place.

   @rtype: list
   @return: list containing the date,max_solar_flux in CSV format.
    One line per item in the list. Solar flux is in watt per meter square.
   """

   nYear = int(crepyscule_tools.ctime_to_iso8601(ctime)[0:4])
   lFlux1 = crepyscule_tools.get_one_year_max_sf(nYear, fLat1)
   lFlux2 = [0] # Initialization to a value to avoid an error for nYMax
   if fLat2 != None: # Draw the other line
      lFlux2 = crepyscule_tools.get_one_year_max_sf(nYear, fLat2)
   else:
      lFlux2 = None

   lDateISO8601 = crepyscule_tools.get_one_year_of_iso8601(ctime)
   lSolarFlux = crepyscule_tools.\
                get_solar_flux_as_csv(lDateISO8601, fLat1, lFlux1, \
                                      fLat2, lFlux2)
   
   return lSolarFlux


def get_max_sf_as_dict(ctime, fLat):
   """
   Return a list containing the maximum solar flux for each day in the
   year where this ctime belongs.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat: float
   @param fLat: Latitude in decimal for the place.

   @rtype: dictionnary
   @return: Dictionnary containing maximum solar flux of each day
    for the year of the ctime. Keys of dictionary is a string in iso8601 format
    like 'YYYY-MM-DD'. Solar flux is in watt per meter square.
   """
   # Use dummy value since it is the same for all lon, crepyscule_summer_time and utc
   sSummerTime = ""
   fLon = 0.0
   fUTC = 0.0
   
   dSF = crepyscule_tools.get_dictionary("sf", fLat, fLon, ctime, \
                                         fUTC, sSummerTime)

   return dSF


def get_max_sf_day(ctime, fLat):
   """
   Get the maximum solar flux value for this day.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat: float
   @param fLat: Latitude in decimal for the place.

   @rtype: float
   @return: Maximum solar flux for this day in float. Solar flux is in
    watt per meter square.
   """
   
   fFlux = crepyscule_tools.get_max_solar_flux(ctime, fLat)

   return fFlux


##############################################################################
# Twilight length.

def get_twilight_length_as_csv(ctime, fLat1, fLat2=None):
   """
   Returns a list containing the date and the twilight length in minute.
   Note: the twilight length is defined as the duration while the sun is below
   the horizon and less than 12° below (nautical twilight).

   See function L{crepyscule_tools.get_twilight_length_as_csv} for
   detailed information.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat1: float
   @param fLat1: Latitude in decimal for the first place.
   @type  fLat2: float
   @param fLat2: Latitude in decimal for the second place.

   @rtype: list
   @return: list containing the date, twilight_length in CSV format.
    One line per item in the list. Twilight length is minutes
   """
   dLength1 = get_twilight_length_as_dict(ctime, fLat1)
   lDate = list(dLength1.keys())
   lDate.sort()

   lLength1 = []
   for i in range(len(lDate)):
      lLength1.append(dLength1[lDate[i]])

   lLength2 = []
   if fLat2 is not None:
      dLength2 = get_twilight_length_as_dict(ctime, fLat2)
      for i in range(len(lDate)):
         lLength2.append(dLength2[lDate[i]])
   else:
      lLength2 = None

   lDateISO8601 = crepyscule_tools.get_one_year_of_iso8601(ctime)
   lTwilightLength = crepyscule_tools.\
                  get_twilight_length_as_csv(lDateISO8601, fLat1, lLength1, \
                                          fLat2, lLength2)

   return lTwilightLength

def get_twilight_length_as_dict(ctime, fLat):
   """
   Return a list containing the twilight length in minute for each day in the
   year where this ctime belongs.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat: float
   @param fLat: Latitude in decimal for the place.

   @rtype: dictionnary
   @return: Dictionnary containing the twilight length of each day
    for the year of the ctime. Keys of dictionary is a string in iso8601 format
    like 'YYYY-MM-DD'. Solar flux is in watt per meter square.
   """
   # Use dummy value since it is the same for all lon, crepyscule_summer_time
   #  and utc
   sSummerTime = ""
   fLon = 0.0
   fUTC = 0.0
   
   dTwilightLength = crepyscule_tools.get_dictionary("twilight", \
                                                     fLat, fLon, ctime, \
                                                     fUTC, sSummerTime )

   return dTwilightLength


def get_twilight_length_day(ctime, fLat):
   """
   Get the twilight length in minute value for this day.

   @type ctime: float
   @param ctime: Any ctime in the year of the result.
   @type  fLat: float
   @param fLat: Latitude in decimal for the place.

   @rtype: float
   @return: Twilight length for this day in float. Length is in degree/hour
   """

   fTwilight_length = crepyscule_tools.get_twilight_length_day(ctime, fLat)

   return fTwilight_length


############################################################
# Crepyscule in Command line
#
#
CFG_LONG_OPTIONS  = ["help","version", \
                     "filename", "ctime", "background-color", \
                     "lat1", "lon1", "utc1", "tz1", \
                     "lat2", "lon2", "utc2", "tz2"]

import argparse

def get_command_line():
   """
   Parse the command line and perform all the checks.
   """
  # Parse the command line
   parser = argparse.ArgumentParser(usage="%(prog)s --filename filename --ctime ctime --background-color bg_color --lat1 latitude1  --lon1 longitude1 --utc1 utc1 --tz1 timezone1")

   parser.add_argument("--version", "-v", dest="version", \
                       help=_("Output version information and exit"),\
                       action="store_true", default=False)
   parser.add_argument("--filename", "-f", dest="filename", \
                     help=_("Filename of the graphic. If not present, the output is forced to text and given in stdout"),\
                       action="store", type=str, default=None)
   parser.add_argument("--ctime", dest="fTime", \
                      help=_("ctime for the current day"),\
                     action="store", type=float, required=True)
   parser.add_argument("--background-color", dest="sBackgroundColor", \
                     help=_("Background color of the graphics"),\
                       action="store", type=str, default="white")
   parser.add_argument("--lat1", dest="fLat1", \
                     help=_("Latitude of the first place"), \
                     action="store", type=float, required=True)
   parser.add_argument("--lon1", dest="fLon1", \
                     help=_("Longitude of the first place. West longitude are negative. East longitude are positive."), \
                     action="store", type=float, required=True)
   parser.add_argument("--utc1", dest="fUTC1", \
                     help=_("Difference of the time zone compared to the UTC time\n for the first place"),\
                     action="store", type=float, required=True)
   parser.add_argument("--tz1", dest="sTZ1", \
                      help=_("Time zone parameter used for summer time. Possible value are:\n IR, JD, EU, IQ, US, CH, EG, MX, SK, HK, NB, FK, PY, NZ, TS, BZ, CL, AU, TG, ZN, LB, SY, RU"),\
                     action="store", type=str, required=True)
   parser.add_argument("--lat2", dest="fLat2", \
                     help=_("Latitude of the second place"),\
                     action="store", type=float)
   parser.add_argument("--lon2", dest="fLon2", \
                     help=_("Longitude of the second place. West longitude are negative.  East longitude are positive."),\
                     action="store", type=float)
   parser.add_argument("--utc2", dest="fUTC2", \
                     help=_("Difference of the time zone compared to the UTC time\n for the second place"), \
                     action="store", type=float)
   parser.add_argument("--tz2", dest="sTZ2", \
                     help=_("Time zone parameter for 2nd place. See --tz1 for possible value"), \
                     action="store", type=str)
   # Type of data wanted by the user
   parser.add_argument("--graph-sunrise-sunset", dest="bGraphSunriseSunset", \
                     help=_("Create the graphic for sunrise/sunset"),\
                     action="store_true", default=False)
   parser.add_argument("--graph-sun-altitude", dest="bGraphSunAltitude", \
                     help=_("Create the graphic for sun altitude"),\
                     action="store_true", default=False)
   parser.add_argument("--graph-daylight-variation", dest="bGraphDaylightVariation", \
                     help=_("Create the graphic for daylight variation"),\
                     action="store_true", default=False)
   parser.add_argument("--graph-solar-flux", dest="bGraphSolarFlux", \
                     help=_("Create the graphic for the solar flux"),\
                     action="store_true", default=False)
   parser.add_argument("--graph-twilight-length", dest="bGraphTwilightLength", \
                     help=_("Create the graphic for the twilight length"), \
                     action="store_true", default=False)
   parser.add_argument("--text-sunrise-sunset", dest="bTextSunriseSunset", \
                     help=_("Create a text file containing the sunrise/sunset value. Omit --filename argument to have the output in stdout."),\
                     action="store_true", default=False)
   parser.add_argument("--graph-all", dest="bGraphAll", \
                     help=_("Create all the graphics for these lat/lon"),\
                     action="store_true", default=False)

   # Parse the args
   options = parser.parse_args()
   
   if options.version:
      print ("crepyscule version: " + str(VERSION))
      print ("Copyright (C) 2014 Free Software Foundation, Inc.")
      print ("License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.")
      print ("This is free software: you are free to change and redistribute it.")
      print ("There is NO WARRANTY, to the extent permitted by law.\n")
      print ("Written by Miguel Tremblay, http://ptaff.ca/miguel/")
      exit(0)

   # Everything is there, check which graphic and text
   dDataRequest = { "GRAPH-SUNRISE" : options.bGraphSunriseSunset,
                    "GRAPH-ALTITUDE" : options.bGraphSunAltitude,
                    "GRAPH-DAYLIGHT_VARIATION" : options.bGraphDaylightVariation,
                    "GRAPH-SOLAR_FLUX" : options.bGraphSolarFlux,
                    "GRAPH-TWILIGHT_LENGTH" : options.bGraphTwilightLength,
                    "TEXT-SUNRISE" : options.bTextSunriseSunset}

   # Check if the filename option is there or not.
   if options.filename is None:
      if dDataRequest["TEXT-SUNRISE"] is False:
         sys.stderr.write( _("ERROR: --filename argument not given. You must ask for a text output (--text-sunrise-sunset) to use stdout. If you want a graphic, please provide a filename.\n"))
         sys.exit(1)
      else: # Do not display any graphs.
         sys.stderr.write(_("INFO: Option --filename not provided. Turning all graphic output to off and using stdout for text output.\n"))
         options.bGraphAll = False
         for sKey in dDataRequest.keys():
            if "GRAPH" in sKey:
               dDataRequest[sKey] = False
   # If all the graphs are asked, put the corresponding boolean to True
   elif options.bGraphAll:    
         for sKey in dDataRequest.keys():
            if "GRAPH" in sKey:
               dDataRequest[sKey] = True

   # Check if at least one graph or text is asked
   i = 0
   for sKey in dDataRequest.keys():
      if dDataRequest[sKey] == False:
         i = i + 1
   if i == len(dDataRequest):
      print (_("WARNING: No data specified, using default (--graph-sunrise-sunset)"))
      print (_("Try --help for more information"))
      dDataRequest["GRAPH-SUNRISE"] = True

   return (dDataRequest, options.fLat1, options.fLon1, options.fTime, options.fUTC1, \
           options.filename, options.sTZ1, options.fLat2, options.fLon2, options.sTZ2, \
           options.fUTC2, options.sBackgroundColor)

if  __name__ == "__main__":

   (bDataRequest, fLat, fLon, fTime, fUTC, sFilename, sTimeZone, fLat2, fLon2, \
    sTimeZone2, fUTC2, sBackgroundColor) = \
    get_command_line()


    
   crepyscule(bDataRequest, fLat, fLon, fTime,\
              fUTC, sFilename, sTimeZone,\
              fLat2, fLon2, sTimeZone2, fUTC2, sBackgroundColor)


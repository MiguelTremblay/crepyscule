��          �   %   �      P     Q     m     q     u     �  #   �     �     �     �     �     �     �     �                    9     =     A     E     Y  #   v     �     �     �    �  %   �                    0  *   4     _  *   ~     �     �     �     �  +   �     �     �     �     	                 *   +      V     w     �     �                                                                         	                                            
         hours of light in all year APR AUG Altitude (degrees) DEC Daily maximal solar flux in Watt/md Daily variation (minute) Daily variation of daylight FEB JAN JUL JUN List must have the same length! MAR MAY Maximum altitude of the sun NOV OCT SEP Solar flux (Watt/m) Sunrise, sunset and daylight Sunrise: %d 
 Sunset: %d 
 Date: %d Time of day Twilight length Twilight length (minute) Project-Id-Version: 0.16
Report-Msgid-Bugs-To: 
POT-Creation-Date: Fri Nov 23 14:02:46 2007
PO-Revision-Date: 2007-11-23 10:40-0500
Last-Translator: Miguel Tremblay <miguel.tremblay@ptaff.ca>
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8bit
 heures de clarté pour toute l'année AVR AOU Altitude (degrés) DEC Flux solaire maximal quotidien en  Watt/mk Variation quotidienne (minute) Variation quotidienne de la durée du jour FEV JAN JUL JUI Les listes doivent avoir la même longueur! MAR MAI Altitude maximale du soleil NOV OCT SEP Flux solaire (Watt/m) Lever, coucher du soleil et durée du jour Aube: %d 
 Aurore: %d 
 Date: %d Heure du jour Durée du crépuscule Durée du crépuscule (minute) 
��          �   %   �      0  "   1      T  n   u  )   �  )     #   8  %   \  %   �  I   �  J   �     =  �   E  Y   �  i   =     �     �  W   �  Y   8  ?   �  �   �     l  B   �     �  y  �  !   c	     �	  �   �	     -
  7   F
  +   ~
  1   �
  &   �
  a     c   e     �  �   �  h   �  �   �     �      �  h   �  h   3  L   �  �   �  &   �  U   �     	                                                                        
          	                                              parameter is missing
 Please try  Background color of the graphics Create a text file containing the sunrise/sunset value. Omit --filename argument to have the output in stdout. Create all the graphics for these lat/lon Create the graphic for daylight variation Create the graphic for sun altitude Create the graphic for sunrise/sunset Create the graphic for the solar flux Difference of the time zone compared to the UTC time
 for the first place Difference of the time zone compared to the UTC time
 for the second place ERROR:  ERROR: --filename argument not given. You must ask for a text output (--text-sunrise-sunset) to use stdout. If you want a graphic, please provide a filename. Filename of the graphic. If not present, the output is forced to text and given in stdout INFO: Option --filename not provided. Turning all graphic output to off and using stdout for text output. Latitude of the first place Latitude of the second place Longitude of the first place. West longitude are negative. East longitude are positive. Longitude of the second place. West longitude are negative.  East longitude are positive. Time zone parameter for 2nd place. See --tz1 for possible value Time zone parameter used for summer time. Possible value are:
 IR, JD, EU, IQ, US, CH, EG, MX, SK, HK, NB, FK, PY, NZ, TS, BZ, CL, AU, TG, ZN, LB, SY, RU Try --help for more information WARNING: No data specified, using default (--graph-sunrise-sunset) ctime for the current day Project-Id-Version: crepyscule_fr
Report-Msgid-Bugs-To: 
POT-Creation-Date: Sun Jan 13 12:18:12 2008
PO-Revision-Date: 2008-01-13 12:24-0500
Last-Translator: Miguel Tremblay <miguel.tremblay@ptaff.ca>
Language-Team:  <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.4
X-Generator: KBabel 1.11.4
  un paramètre est manquant
 Svp  Couleur de fond des graphiques Cree un fichier texte contenant les valeurs de lever/coucher du soleil. L'omission de l'argument --filename donne la sortie dans stdout. Cree tous les graphiques Cree le graphique pour la variation de la duree du jour Cree le graphique pour l'altitude du soleil Cree le graphique pour le lever/coucher du soleil Cree le graphique pour le flux solaire Nombre d'heures de difference du fuseau horaire compare au temps UTC
 pour le premier emplacement Nombre d'heures de difference du fuseau horaire compare au temps UTC
 pour le deuxieme  emplacement ERREUR ERREUR: --filename absent des arguments. Vous devez demande une sortie texte (--text-sunrise-sunset) pour utiliser la sortie stdout. Si vous desirez un graphique veuillez fournir un nom de fichier. Nom du fichier pour le graphique. En cas d'absence, la sortie est forcee au texte et donnee dans stdout. INFO: l'argument  --filename n'est pas fourni. Toutes les sorties graphiques sont desactivees et stdout est utilise pour la sortie texte. Latitude du premier emplacement Latitude du deuxieme emplacement Longitude du premier emplacement. Les longitude ouest sont négatives. Les longitude est sont positives. Longitude du premier emplacement. Les longitude ouest sont négatives. Les longitude est sont positives. Fuseau horaire du deuxieme emplacement. Cf. --tz1 pour les valeurs possibles Code du fuseau horaire pour l'heure d'ete. Les valeurs possibles sont:
 IR, JD, EU, IQ, US, CH, EG, MX, SK, HK, NB, FK, PY, NZ, TS, BZ, CL, AU, TG, ZN, LB, SY, RU Essayer --help pour plus d'information AVERTISSEMENT: Aucune donnee specifie, utilisation du defaut (--graph-sunrise-sunset) ctime pour le jour courant 
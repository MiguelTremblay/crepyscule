��          �   %   �      0  "   1      T  n   u  )   �  )     #   8  %   \  %   �  I   �  J   �     =  �   E  Y   �  i   =     �     �  W   �  Y   8  ?   �  �   �     l  B   �     �  _  �  "   I	      l	  n   �	  )   �	  )   &
  #   P
  %   t
  %   �
  I   �
  J   
     U  �   ]  Y   �  i   U     �     �  W   �  Y   P  ?   �  �   �     �  B   �     �                                                                        
          	                                              parameter is missing
 Please try  Background color of the graphics Create a text file containing the sunrise/sunset value. Omit --filename argument to have the output in stdout. Create all the graphics for these lat/lon Create the graphic for daylight variation Create the graphic for sun altitude Create the graphic for sunrise/sunset Create the graphic for the solar flux Difference of the time zone compared to the UTC time
 for the first place Difference of the time zone compared to the UTC time
 for the second place ERROR:  ERROR: --filename argument not given. You must ask for a text output (--text-sunrise-sunset) to use stdout. If you want a graphic, please provide a filename. Filename of the graphic. If not present, the output is forced to text and given in stdout INFO: Option --filename not provided. Turning all graphic output to off and using stdout for text output. Latitude of the first place Latitude of the second place Longitude of the first place. West longitude are negative. East longitude are positive. Longitude of the second place. West longitude are negative.  East longitude are positive. Time zone parameter for 2nd place. See --tz1 for possible value Time zone parameter used for summer time. Possible value are:
 IR, JD, EU, IQ, US, CH, EG, MX, SK, HK, NB, FK, PY, NZ, TS, BZ, CL, AU, TG, ZN, LB, SY, RU Try --help for more information WARNING: No data specified, using default (--graph-sunrise-sunset) ctime for the current day Project-Id-Version: crepyscule
POT-Creation-Date: Sun Jan 13 12:18:12 2008
PO-Revision-Date: 2008-02-13 11:09-0500
Last-Translator: Miguel Tremblay <miguel.tremblay@ptaff.ca>
Language-Team:  <en@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: KBabel 1.11.4
  parameter is missing
 Please try  Background color of the graphics Create a text file containing the sunrise/sunset value. Omit --filename argument to have the output in stdout. Create all the graphics for these lat/lon Create the graphic for daylight variation Create the graphic for sun altitude Create the graphic for sunrise/sunset Create the graphic for the solar flux Difference of the time zone compared to the UTC time
 for the first place Difference of the time zone compared to the UTC time
 for the second place ERROR:  ERROR: --filename argument not given. You must ask for a text output (--text-sunrise-sunset) to use stdout. If you want a graphic, please provide a filename. Filename of the graphic. If not present, the output is forced to text and given in stdout INFO: Option --filename not provided. Turning all graphic output to off and using stdout for text output. Latitude of the first place Latitude of the second place Longitude of the first place. West longitude are negative. East longitude are positive. Longitude of the second place. West longitude are negative.  East longitude are positive. Time zone parameter for 2nd place. See --tz1 for possible value Time zone parameter used for summer time. Possible value are:
 IR, JD, EU, IQ, US, CH, EG, MX, SK, HK, NB, FK, PY, NZ, TS, BZ, CL, AU, TG, ZN, LB, SY, RU Try --help for more information WARNING: No data specified, using default (--graph-sunrise-sunset) ctime for the current day 
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2005,2008  Miguel Tremblay

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not see  <http://www.gnu.org/licenses/>.
############################################################################

"""
Package that contains everything to plot graphics for crepyscule.
Based on U{rpy<http://rpy.sourceforge.net/>} and
U{Cairo<https://cran.r-project.org/web/packages/Cairo/index.html>} (U{R language<http://ptaff.ca/crepyscule/>}). All images are in the png format.

Project home page is U{http://ptaff.ca/crepyscule/}
           
 - Author:      U{Miguel Tremblay<http://ptaff.ca/miguel/>}
 - Date:        February 20th  2005
 - Name:        crepyscule_graphic.py
"""

import sys
import time
import datetime
import calendar

import numpy

# rpy2
import rpy2.rpy_classic as rpy
from rpy2.robjects.packages import importr
r_base = importr('base')
grdevices = importr('grDevices')
graphics = importr('graphics')


import crepyscule_tools

# Set the place where to fetch languages dependant messages
import gettext
t = gettext.translation('crepyscule_graphic', sys.path[0] + '/locale')
_ = t.gettext


# Graphics element used in both day by day and month plot.
lMonthString = [_('JAN'), _('FEB'), _('MAR'), \
                _('APR'), _('MAY'), _('JUN'),\
                _('JUL'), _('AUG'), _('SEP'),\
                _('OCT'), _('NOV'), _('DEC')]
nPngWidth = 640
nPngHeight =480
rlMonth = None
rX_axis = None
sColor = 'white' #'transparent'
sFirstCityColor = 'blue'
sSecondCityColor = 'limegreen'

def init_time_values(lDateISO8601):
    """
    Set the value of time used by the graphs.
    Declare the import here because a lot of output are written when it
    is done and we do not want to have it every time crepyscule is invoked,
    only when this module is called.
   
    @type lDateISO8601: liste
    @param lDateISO8601: Create the X axis based on the values of the list.
     Values are one year date in iso8601 format.
    """
    global lMonthString, nPngWidth, nPngHeight
    global rlMonth, sColor, rX_axis
    global rpy2
    import rpy2

    package_cairo = importr("Cairo")
    
    if rX_axis == None:
        rX_axis = r_base.strptime(lDateISO8601, "%Y-%m-%d")           
        rlRange_year = r_base.range(rX_axis) # X
        # Ugly conversion
        rPOSIXct= r_base.as_POSIXct(rlRange_year, format="%Y-%m-%d")
        sStart = datetime.datetime.fromtimestamp(rPOSIXct[0]).strftime('%Y-%m-%d')
        sEnd= datetime.datetime.fromtimestamp(rPOSIXct[1]).strftime('%Y-%m-%d')
        rlMonth = r_base.seq(r_base.strptime(sStart, "%Y-%m-%d"), r_base.strptime(sEnd, "%Y-%m-%d"), by="months")
        
def plot_altitude(tToday, lSunAltitude, lDateISO8601, sFilename,\
                  sBackgroundColor, lSunAltitude2=None):
    """
    Plot the altitude of the sun during a year.

    @type tToday: tuple
    @param tToday: Tuple as returned by the function time.gmtime.
    @type lSunAltitude: list
    @param lSunAltitude: list of Sun altitude for the first place.
    @type lDateISO8601: list
    @param lDateISO8601: List of strings for one year in iso8601 format.
    @type sFilename: string
    @param sFilename: Filename to save the graph. Filename is transformed like
     'file.png' to 'file_alt.png'.
    @type sBackgroundColor: string
    @param sBackgroundColor: Color that will be used for the background
    color for the generated graphics. See U{Chart of R colors<http://research.stowers-institute.org/efg/R/Color/Chart/>}
    @type lSunAltitude2: list
    @param lSunAltitude2:  list of Sun altitude for the second place.
    """

    init_time_values(lDateISO8601)
    sFilenameAltitude = crepyscule_tools.remove_extension(sFilename) +\
                        '_alt.png'
    sTitlePlot = _("Maximum altitude of the sun")

    grdevices.png(sFilenameAltitude, type='cairo-png', w = nPngWidth, h = nPngHeight,\
                             bg='white', pointsize=8)

    graphics.par(las=1)

    graphics.plot(rX_axis, lSunAltitude,  ylim=r_base.c(0, 90), xaxs='i',
           lty=0, type='l', bg='blue',\
           xlab='Date', ylab=_('Altitude (degrees)'), main=sTitlePlot,
           xaxt='n', yaxt='n')
    lUsr = graphics.par("usr")

    
    ## Axis
    graphics.axis(2, r_base.range(0,91,5)) # Y
    graphics.axis(1, at=rlMonth, labels=lMonthString)
    ### Grid
    rXdomain = r_base.c(lUsr[0], lUsr[1])
    for i in range(0, 91, 5):
        rYdomain = r_base.c(i,i)
        graphics.lines(rXdomain, rYdomain, col='gray70') # Horizontal lines
    rYdomain = r_base.c(lUsr[2], lUsr[3])
    for rnMonth in rlMonth:
        rXdomain = r_base.c(rnMonth, rnMonth)
        graphics.lines(rXdomain, rYdomain, col='gray70') # Vertical lines
    graphics.lines( r_base.c(rlMonth[0], rlMonth[0]), rYdomain, col='black')
    # Draw altitude
    graphics.lines(rX_axis, lSunAltitude, col=sFirstCityColor, lwd=2)

    if lSunAltitude2 != None:
        graphics.lines(rX_axis, lSunAltitude2, col=sSecondCityColor, lwd=2)
    else:
        fAltitude = lSunAltitude[tToday[7]]
        __draw_today(tToday, fAltitude, round(fAltitude), 45)

    grdevices.dev_off()
    print ("filename", sFilenameAltitude)

def __draw_today_sun(tToday, fSunriseTime, fSunsetTime):
    """
    Add information about current day on the graphics of sunset and sunrise.
    Draw a vertical line for the current date.
    Draw a circle at the place where this vertical line cross each of the
    three lines (sunset, sunrise, total day light).
    Write the value of at these circles.

    @type tToday: tuple
    @param tToday: Tuple as returned by the function time.gmtime.
    @type fSunsetTime: float
    @param fSunsetTime: Time of sunset.
    @type fSunriseTime: float
    @param fSunriseTime: Time of sunrise.
    """
    lUsr = graphics.par('usr')
    if fSunsetTime > fSunriseTime:
        fTotalTime = fSunsetTime - fSunriseTime
    else:
        fTotalTime = fSunriseTime - fSunsetTime

    # Ugly, ugly way to get a date. Try to be more elegant. Please.
    rToday = r_base.strptime(time.strftime("%Y-%m-%d", tToday), "%Y-%m-%d")
    cTimeToday = time.mktime(tToday)

    # Vertical line ####################
    rXdomain = r_base.c(rToday, rToday)
    rYdomain = r_base.c(lUsr[2], lUsr[3])
    graphics.lines(rXdomain, rYdomain, col='gray20', lty='dashed')
    # Circles. Text only use ctime... ############
    graphics.points(cTimeToday, fSunsetTime, lwd=2, pch=19)
    graphics.points(cTimeToday, fSunriseTime, lwd=2, pch=19)
    graphics.points(cTimeToday, fTotalTime, lwd=2, pch=19)
    # Text #########
    tSunrise = crepyscule_tools.tranform_decimal_hour_in_minutes(\
               fSunriseTime)
    tSunset = crepyscule_tools.tranform_decimal_hour_in_minutes(\
              fSunsetTime)
    tTotalTime = crepyscule_tools.tranform_decimal_hour_in_minutes(\
                 fTotalTime)
    sSunrise = tSunrise[0] + ':' + tSunrise[1]
    sSunset = tSunset[0] + ':' + tSunset[1]
    sTotalTime = tTotalTime[0] + ':' + tTotalTime[1] 

    # Position of text for sunrise time
    # Standard position is BELOW
    ABOVE = 3
    BELOW = 1
    nPosTotal = nPosSunrise = nPosSunset = BELOW
    if fSunriseTime < 1 :
        nPosSunrise = ABOVE
    fDiffTotalSunrise = fTotalTime - fSunriseTime
    if fDiffTotalSunrise > 0 and fDiffTotalSunrise < 2:
        nPosTotal = ABOVE
    elif fDiffTotalSunrise < 0 and abs(fDiffTotalSunrise) < 2:
        nPosSunrise = ABOVE

    graphics.text(cTimeToday, fSunriseTime, labels=sSunrise, pos=nPosSunrise, \
                       xpd=True)
    graphics.text(cTimeToday, fSunsetTime, labels=sSunset, pos=nPosSunset, \
                       xpd=True)    
    graphics.text(cTimeToday, fTotalTime, labels=sTotalTime, pos=nPosTotal, \
                       xpd=True)    

def __draw_today(tToday, fTodayValue, sLabel, fMiddleOfGraph):
    """
    Draw a line in the graphic for the value of the current day.
    NOT for sunrise/sunset. See L{__draw_today_sun} in this case.
    X{private}

    @type tToday: tuple
    @param tToday: Tuple as returned by the function time.gmtime.
    @type fTodayValue: float
    @param fTodayValue: Value to be mark on the curve.
    @type sLabel: string
    @param sLabel: Label to be written at the position of the mark.
    @type fMiddleOfGraph: float
    @param fMiddleOfGraph: Middle of the Y axis. Used to know if the label
     goes over or under the bullet on the curve.
    """

    lUsr = graphics.par('usr')
    rToday = r_base.strptime(time.strftime("%Y-%m-%d", tToday), "%Y-%m-%d")
    cTimeToday = time.mktime(tToday)


    # Vertical line ####################
    rXdomain = r_base.c(rToday, rToday)
    rYdomain = r_base.c(lUsr[2], lUsr[3])
    graphics.lines(rXdomain, rYdomain, col='gray20', lty='dashed')
    # Circles. Text only use ctime... ############
    graphics.points(cTimeToday, fTodayValue, lwd=2, pch=19)
    # Position of text for sunrise time
    # Standard position is BELOW
    ABOVE = 3
    BELOW = 1
    if fTodayValue < fMiddleOfGraph :
        nPos = ABOVE
    else:
        nPos = BELOW

    graphics.text(cTimeToday, fTodayValue, labels=sLabel, pos=nPos,  \
                       xpd=True)    


def plot_sunset_sunrise(lSunrise, lSunset, lDaylength, lDateISO8601,\
                        sFilename, tToday, sBackgroundColor="white", \
                        lSunrise2=None, lSunset2=None, lDaylength2=None):
    """
    Plot the sunset and the sunrise of one year.

    @type lSunrise: list
    @param lSunrise: List of sunrise values for one year for the first place.
    @type lSunset: list
    @param lSunset: List of sunset values for one year for the first place.
    @type lDaylength: list
    @param lDaylength: List of day length for each day of the year.
    @type lDateISO8601: list
    @param lDateISO8601: List of strings for one year in iso8601 format.
    @type sFilename: string
    @param sFilename: Filename to save the graph.
    @type tToday: tuple
    @param tToday: Tuple as returned by the function time.gmtime.
    @type sBackgroundColor: string
    @param sBackgroundColor: Color that will be used for the background
     color for the generated graphics. See U{Chart of R colors<http://research.stowers-institute.org/efg/R/Color/Chart/>}
    @type lSunrise2: list
    @param lSunrise2: List of sunrise values for one year for the second place.
    @type lSunset2: list
    @param lSunset2: List of sunset values for one year for the second place.
    @type lDaylength2 list
    @param lDaylength2: List of day length for each day of the year for the
     second place.
    """
    init_time_values(lDateISO8601)
    sTitle = _("Sunrise, sunset and daylight")

    print (sFilename, nPngWidth,  nPngHeight,\
                     sBackgroundColor)
    grdevices.png(sFilename, type='cairo-png', w = nPngWidth, h = nPngHeight,\
                     bg=sBackgroundColor, pointsize=8)
    if len(lSunrise) != len(lSunset) != len(lDateISO8601):
        print (_("List must have the same length!"))
        print (_("Sunrise: %d \n Sunset: %d \n Date: %d")) % \
              (len(lSunrise), len(lSunset), len(lDateISO8601))
        return

    # Use the current julian day to get sunrise/sunset for today
    fTodaySunriseTime = lSunrise[tToday[7]-1]
    fTodaySunsetTime = lSunset[tToday[7]-1]
    
    # Bugs of Sun.py patch
    (nIndiceStartSunset, nIndiceEndSunset) = crepyscule_tools.\
                                             check_value_over_24(lSunset)
    (nIndiceStartSunrise, nIndiceEndSunrise) = crepyscule_tools.\
                                               check_value_under_0(lSunrise)
    (npSunrise, npSunset) =  crepyscule_tools.\
                            correct_sun_bugs(lSunrise, lSunset)

    if lSunrise2 != None and lSunset2 != None:
        (nIndiceStartSunset2, nIndiceEndSunset2) = \
                              crepyscule_tools.check_value_over_24(lSunset2)
        (nIndiceStartSunrise2, nIndiceEndSunrise2) = \
                               crepyscule_tools.check_value_under_0(lSunrise2)

        (npSunrise2, npSunset2) = crepyscule_tools.\
                                  correct_sun_bugs(lSunrise2, lSunset2)

    graphics.par(las=1)
    sYLabel = _("Time of day")

    graphics.plot(rX_axis, lSunrise, ylim=r_base.c(1, 23.5), xaxs='i', \
           lty=0, type='l',\
           xlab='Date', ylab=sYLabel, main=sTitle, xaxt='n', yaxt='n')
    lUsr = graphics.par("usr")
    ## Axis
    graphics.axis(2, r_base.range(0,24)) # Y
    graphics.axis(1, at=rlMonth, labels=lMonthString)
    
    ### Grid
    rXdomain = r_base.c(lUsr[0], lUsr[1])
    for i in range(1, 25):
        rYdomain = r_base.c(i,i)
        graphics.lines(rXdomain, rYdomain, col='gray70') # Horizontal lines
    rYdomain = r_base.c(lUsr[2], lUsr[3])
    for rnMonth in rlMonth:
        rXdomain = r_base.c(rnMonth, rnMonth)
        graphics.lines(rXdomain, rYdomain, col='gray70') # Vertical lines
    graphics.lines( r_base.c(rlMonth[0], rlMonth[0]), rYdomain, col='black')

    draw_sunrise_sunset(npSunrise, npSunset, lDaylength,\
                        nIndiceStartSunrise, nIndiceEndSunrise,\
                        nIndiceStartSunset, nIndiceEndSunset, lDateISO8601)

    if lSunrise2 == None:
        __draw_today_sun(tToday, fTodaySunriseTime,fTodaySunsetTime)

    else:
        draw_sunrise_sunset(npSunrise2, npSunset2, lDaylength2, \
                            nIndiceStartSunrise2, nIndiceEndSunrise2,\
                            nIndiceStartSunset2, nIndiceEndSunset2,\
                            lDateISO8601,\
                            sColorSunrise='maroon', sColorSunset='saddlebrown',\
                            sColorTotalTime='purple')

    
    grdevices.dev_off()

def plot_monthly(lMonthLight, sFilename):
    """
    DEPRECATED
    Plot a graphical with total hour of light for each month. Was used
    for debugging and development phase. Probably won't work. Code source
    is left here in case someone wants to use it. If it is your case, please
    U{contact the author<mailto:miguel.tremblay@ptaff.ca>}.

    @type lMonthLight: list
    @param lMonthLight: list containing the sum of day light for each month.
    @type sFilename: string
    @param sFilename: Filename to save the graph. Filename is transformed like
     'file.png' to 'file_month.png'.
    """
    init_time_values(lDateISO8601)

    sFilenameMonth = remove_extension(sFilename) + '_month.png'
    grdevices.png(sFilenameMonth, nPngWidth, nPngHeight, bg=sColor)
    graphics.par(las=1)
    rMidPoint = graphics.barplot(lMonthLight, names=lMonthString, \
                          col='red', ylim=r_base.c(0,max(lMonthLight)+100))
    lUsr = graphics.par('usr')
    # Print the total number of hours on top of each bar
    for i in range(0,len(rMidPoint)):
        if int(lMonthLight[i]) != 0:
            lrange = calendar.monthrange(2005, i+1)
            graphics.axis(1, at=rMidPoint[i], \
                   labels=round(lMonthLight[i]/lrange[1],1), tick=0,\
                   pos=lMonthLight[i]+(lUsr[3].as_py(rpy.BASIC_CONVERSION))/10)
    # Write the total number of hours
    sTotalHour = str(int(numpy.array(lMonthLight).sum())) + \
                 _(" hours of light in all year")
    graphics.text((lUsr[0].as_py(rpy.BASIC_CONVERSION)+\
            lUsr[1].as_py(rpy.BASIC_CONVERSION))/2, \
           lUsr[3], labels=sTotalHour, pos=1)
    grdevices.dev_off()



def draw_sunrise_sunset(npSunrise, npSunset, npSunTime,\
                        nIndiceStartSunrise, nIndiceEndSunrise, \
                        nIndiceStartSunset, nIndiceEndSunset, \
                        lDateISO8601, sLty='solid',
                        sColorSunrise='orange', sColorSunset='red', \
                        sColorTotalTime='blue'):
    """
    Draw sunrise and sunset lines on the graphics. This function is empiric.
    It has been developed by trials and errors, there is not much to
    understand.

    There is only two complex case that are handled: when the sunrise (sunset)
    curve goes under (over) zero (24) value during one segment. In this case,
    there is 3 curves that are created corresponding to the value of
    rX_axis1, rX_axis2 and rX_axis3.

    @type npSunrise: numpy.array
    @param npSunrise: Array of sunrise values for one year to be displayed.
    @type npSunset: numpy.array
    @param npSunset: Array of sunset values for one year to be displayed.
    @type nIndiceStartSunrise: int
    @param nIndiceStartSunrise: Indice where the sunrise goes under the zero
     value. If there is no such place, zero is given.
    @type nIndiceEndSunrise: int
    @param nIndiceEndSunrise: Indice where the sunrise goes over zero if it
     ever goes under it. If it does not occurs, the length of the array
     (365 or 366) is given.
    @type nIndiceStartSunset: int
    @param nIndiceStartSunset:  Indice where the sunrise goes over 24
     value. If there is no such place, zero is given.
    @type nIndiceEndSunset: int
    @param nIndiceEndSunset: Indice where the sunset goes under 24 if it
     ever goes over it. If it does not occurs, the length of the array
     (365 or 366) is given.
    @type lDateISO8601: list
    @param lDateISO8601: List of strings for one year in iso8601 format.
    @type sLty: string
    @param sLty: R Line style. See help for 'lty' in the 'par' documentation
     in R
    @type sColorSunrise: string
    @param sColorSunrise: Color of sunrise curve. See U{Chart of R colors<http://research.stowers-institute.org/efg/R/Color/Chart/>} for possible values.
    @type sColorSunset: string
    @param sColorSunset: Color of sunset curve. See U{Chart of R colors<http://research.stowers-institute.org/efg/R/Color/Chart/>} for possible values.
    @type sColorTotalTime: string
    @param sColorTotalTime:  Color of total time of day curve. See U{Chart of R colors<http://research.stowers-institute.org/efg/R/Color/Chart/>} for possible values.
    
    """
    ####
    # Sunrise
    if nIndiceStartSunrise == 0:
        graphics.lines(rX_axis, npSunrise.tolist(), type='l', col=sColorSunrise, lwd=2, lty=sLty)
    elif nIndiceStartSunrise < nIndiceEndSunrise: # Northern hemisphere
        rX_axis1 = r_base.strptime(lDateISO8601[0:nIndiceStartSunrise], "%Y-%m-%d")
        rX_axis2 = r_base.strptime(lDateISO8601[nIndiceStartSunrise:nIndiceEndSunrise], \
                              "%Y-%m-%d")
        rX_axis3 = r_base.strptime(lDateISO8601[nIndiceEndSunrise:], "%Y-%m-%d")
        graphics.lines(rX_axis1, npSunrise[0:nIndiceStartSunrise], \
                type='l', col=sColorSunrise, lwd=2, lty=sLty)
        graphics.lines(rX_axis2, npSunrise[nIndiceStartSunrise:nIndiceEndSunrise], type='l',\
                col=sColorSunrise, lwd=2, lty='dashed')
        graphics.lines(rX_axis3, npSunrise[nIndiceEndSunrise:], type='l', col=sColorSunrise, lwd=2, lty=sLty)
    else : # Southern hemisphere
        rX_axis1 = r_base.strptime(lDateISO8601[0:nIndiceEndSunrise], "%Y-%m-%d")
        rX_axis2 = r_base.strptime(lDateISO8601[nIndiceEndSunrise:nIndiceStartSunrise], \
                              "%Y-%m-%d")
        rX_axis3 = r_base.strptime(lDateISO8601[nIndiceStartSunrise:], "%Y-%m-%d")
        graphics.lines(rX_axis1, npSunrise[0:nIndiceEndSunrise], \
                type='l', col=sColorSunrise, lwd=2, lty='dashed')
        graphics.lines(rX_axis2, npSunrise[nIndiceEndSunrise:nIndiceStartSunrise], type='l',\
                col=sColorSunrise, lwd=2, lty=sLty)
        graphics.lines(rX_axis3, npSunrise[nIndiceStartSunrise:], type='l', \
                col=sColorSunrise, lwd=2, lty='dashed')
    # Sunset
    if nIndiceStartSunset == 0:
        graphics.lines(rX_axis, npSunset.tolist(), type='l', col=sColorSunset, lwd=2, lty=sLty)
    elif nIndiceStartSunset < nIndiceEndSunset: # Northern hemisphere
        rX_axis1 = r_base.strptime(lDateISO8601[0:nIndiceStartSunset], "%Y-%m-%d")
        rX_axis2 = r_base.strptime(lDateISO8601[nIndiceStartSunset:nIndiceEndSunset], \
                              "%Y-%m-%d")
        rX_axis3 = r_base.strptime(lDateISO8601[nIndiceEndSunset:], "%Y-%m-%d")
        graphics.lines(rX_axis1, npSunset[0:nIndiceStartSunset], \
                type='l', col=sColorSunset, lwd=2, lty=sLty)
        graphics.lines(rX_axis2, npSunset[nIndiceStartSunset:nIndiceEndSunset], type='l',\
                col=sColorSunset, lwd=2, lty='dashed')
        graphics.lines(rX_axis3, npSunset[nIndiceEndSunset:], type='l', col=sColorSunset, lwd=2, lty=sLty)
    else : # Southern hemisphere
        rX_axis1 = r_base.strptime(lDateISO8601[0:nIndiceEndSunset], "%Y-%m-%d")
        rX_axis2 = r_base.strptime(lDateISO8601[nIndiceEndSunset:nIndiceStartSunset], \
                              "%Y-%m-%d")
        rX_axis3 = r_base.strptime(lDateISO8601[nIndiceStartSunset:], "%Y-%m-%d") 
        graphics.lines(rX_axis1, npSunset[0:nIndiceEndSunset], \
                type='l', col=sColorSunset, lwd=2, lty='dashed')
        graphics.lines(rX_axis2, npSunset[nIndiceEndSunset:nIndiceStartSunset], type='l',\
                col=sColorSunset, lwd=2, lty=sLty)
        graphics.lines(rX_axis3, npSunset[nIndiceStartSunset:], type='l', \
                col=sColorSunset, lwd=2, lty='dashed')
        
    graphics.lines(rX_axis, npSunTime, type='l', col=sColorTotalTime, lwd=2, lty=sLty)


def plot_daylight_differences(lSunrise1, lSunset1, lDaylength1, lDateISO8601,\
                              sFilename, tToday, \
                              sBackgroundColor, fDaylengthDecember31stLastYear,  \
                              lSunrise2=None, lSunset2=None, lDaylength2=None, \
                              fDaylengthDecember31stLastYear2 = None):
    """
    Plot the difference of daylight time between consecutive days.

    @type lSunrise1: list
    @param lSunrise1: List of sunrise values for one year for the first place.
    @type lSunset1: list
    @param lSunset1: List of sunset values for one year for the first place.
    @type lDaylength1: list
    @param lDaylength1: List of day length for each day of the year.
    @type lDateISO8601: list
    @param lDateISO8601: List of strings for one year in iso8601 format.
    @type sFilename: string
    @param sFilename: Filename to save the graph. Filename is transformed like
     'file.png' to 'file_delta_t.png'.
    @type tToday: tuple
    @param tToday: Tuple as returned by the function time.gmtime.
    @type sBackgroundColor: string
    @param sBackgroundColor: Color that will be used for the background
     color for the generated graphics. See U{Chart of R colors<http://research.stowers-institute.org/efg/R/Color/Chart/>}
    @type lSunrise2: list
    @param lSunrise2: List of sunrise values for one year for the second place.
    @type lSunset2: list
    @param lSunset2: List of sunset values for one year for the second place.
    @type lDaylength2 list
    @param lDaylength2: List of day length for each day of the year for the
     second place.
    """
    init_time_values(lDateISO8601)

    # Use the current julian day to get sunrise/sunset for today
    fTodaySunriseTime = lSunrise1[tToday[7]-1]
    fTodaySunsetTime = lSunset1[tToday[7]-1]
    
    sFilenameTimeDifference = crepyscule_tools.\
                              remove_extension(sFilename) +\
                              '_delta_t.png'

    sTitle = _("Daily variation of daylight")
    
    package_cairo = importr("Cairo")
    grdevices.png(sFilenameTimeDifference, type='cairo-png', w = nPngWidth, \
          h = nPngHeight, bg=sBackgroundColor, pointsize=8)

    lDiffSunTime = crepyscule_tools.get_daylight_variation(lDaylength1,\
                            fDaylengthDecember31stLastYear)
    lDiffSunTime2 = []
    if lSunrise2 != None and lSunset2 != None:
        lDiffSunTime2 = crepyscule_tools.get_daylight_variation(lDaylength2, \
            fDaylengthDecember31stLastYear2)
    
    # Labeling on Y axis
    nYMax = int(round(max(lDiffSunTime+lDiffSunTime2)+0.5))
    nYMin = int(round(min(lDiffSunTime+lDiffSunTime2)-0.5))
    npbsMax = max(nYMax, abs(nYMin))
    # Zero must be have a tick
    lYAxisDivisionUp = numpy.arange(0, npbsMax+(nYMax-nYMin)/10.0,\
                        (nYMax-nYMin)/10.0).tolist()
    lYAxisDivisionDown = numpy.arange(0, -npbsMax-(nYMax-nYMin)/10.0, \
                          -(nYMax-nYMin)/10.0).tolist()
    # Remove the 0 from one of the 2 arrays    
    lYAxisDivisionDown.pop(0)
    # use a list to append the numpy
    npYAxisDivision = numpy.array(lYAxisDivisionDown + \
                      lYAxisDivisionUp)

    graphics.par(mar=r_base.c(5,7,4,2)) # Put more space at the left of Y axis
    graphics.par(las=1)
    graphics.plot(rX_axis, lDiffSunTime, ylim=r_base.c(nYMin, nYMax), xaxs='i', \
           lty=0, type='l',\
           xlab='Date', ylab='', main=sTitle, xaxt='n', yaxt='n')

    # Y label
    sYLabel = _("Daily variation (minute)")
    graphics.par(las=0) # Put text vertical
    graphics.mtext(sYLabel, side=2, line=6)
    graphics.par(las=1)
    
    lUsr = graphics.par("usr")
    
    ## Axis
    graphics.axis(2, npYAxisDivision.tolist(), labels=numpy.around(npYAxisDivision,2).tolist()) # Y
    graphics.axis(1, at=rlMonth, labels=lMonthString)
    ### Grid
    rXdomain = r_base.c(lUsr[0], lUsr[1])
    for i in npYAxisDivision:
        rYdomain = r_base.c(i,i)
        graphics.lines(rXdomain, rYdomain, col='gray70') # Horizontal lines
    rYdomain = r_base.c(lUsr[2], lUsr[3])
    for rnMonth in rlMonth:
        rXdomain = r_base.c(rnMonth, rnMonth)
        graphics.lines(rXdomain, rYdomain, col='gray70') # Vertical lines
    graphics.lines( r_base.c(rlMonth[0], rlMonth[0]), rYdomain, col='black')
    graphics.lines(rX_axis, lDiffSunTime, type='l', col=sFirstCityColor, lwd=1)
    # Draw the line for the second city...
    if lSunrise2 != None and lSunset2 != None:
        graphics.lines(rX_axis, lDiffSunTime2, type='l', col=sSecondCityColor, lwd=1)
    else: # Or the difference for today
        fDiffToday = lDiffSunTime[tToday[7]-1]
        fDiffTodayMinutes = round(fDiffToday,2)
        __draw_today(tToday, fDiffToday, fDiffTodayMinutes, 0)
         
    grdevices.dev_off()
    print ("filename", sFilenameTimeDifference)


def plot_max_solar_flux(lDateISO8601, sFilename, tToday, sBackgroundColor, \
                        fLat1, fLat2=None):
    """
    Plot the maximal solar flux in W/m^2 for all year.

    @type lDateISO8601: list
    @param lDateISO8601: List of strings for one year in iso8601 format.
    @type sFilename: string
    @param sFilename: Filename to save the graph. Filename is transformed like
     'file.png' to 'file_sf.png'.
    """
    init_time_values(lDateISO8601)
    
    # Cairo and filename stuff
    sFilenameSolarFlux = crepyscule_tools.\
                         remove_extension(sFilename) +\
                         '_sf.png'
    sTitle = _("Daily maximal solar flux in Watt/m") + "²"
    package_cairo = importr("Cairo")
    grdevices.png(sFilenameSolarFlux, type='cairo-png', w = nPngWidth, \
          h = nPngHeight, bg=sBackgroundColor, pointsize=8)


    # Get the solar flux for all the year for the first latitude
    nYear = int(lDateISO8601[0][0:4])
    lFlux1 = crepyscule_tools.get_one_year_max_sf(nYear, fLat1)
    lFlux2 = [0] # Initialization to a value to avoid an error for nYMax
    if fLat2 != None: # Draw the other line
        lFlux2 = crepyscule_tools.get_one_year_max_sf(nYear, fLat2)
    
    # Labeling on Y axis
    sYLabel = _("Solar flux (Watt/m") + "²"
    nYMax = int(max(max(lFlux1),max(lFlux2)))

    # Try 10 horizontal lines
    if nYMax > 500:
        nVerticalIncrement = 100
    else:
        nVerticalIncrement = 50

    # Plot the fluxes
    graphics.par(las=1)
    graphics.plot(rX_axis, lFlux1, ylim=r_base.c(0, nYMax), xaxs='i', yaxs='r',\
           lty=0, type='l',\
           xlab='Date', ylab=sYLabel, main=sTitle, xaxt='n', yaxt='n')
    lUsr = graphics.par("usr")
    graphics.axis(1, at=rlMonth, labels=lMonthString)
    graphics.axis(2, at=r_base.range(0, nYMax, nVerticalIncrement))
    ### Grid
    rXdomain = r_base.c(lUsr[0], lUsr[1])
    for i in range(0, nYMax, nVerticalIncrement):
        rYdomain = r_base.c(i,i)
        graphics.lines(rXdomain, rYdomain, col='gray70') # Horizontal lines
    rYdomain = r_base.c(lUsr[2], lUsr[3])
    for rnMonth in rlMonth:
        rXdomain = r_base.c(rnMonth, rnMonth)
        graphics.lines(rXdomain, rYdomain, col='gray70') # Vertical lines
    graphics.lines( r_base.c(rlMonth[0], rlMonth[0]), rYdomain, col='black')

    graphics.lines(rX_axis, lFlux1, col='red')
    if fLat2 != None:
        graphics.lines(rX_axis, lFlux2, col='orange')
    else: # Draw the value for today
        fTodayValue = lFlux1[tToday[7]-1]
        sLabel = str(int(round(fTodayValue)))
        npFlux1 = numpy.array(lFlux1)
        fMiddleOfGraph = npFlux1.mean()
        __draw_today(tToday, fTodayValue, sLabel, fMiddleOfGraph)
    
    grdevices.dev_off()

    print ('filename', sFilenameSolarFlux)


def plot_twilight_length(sFilename, tToday, sBackgroundColor, \
                         fLat1, dTwilightLength1, fLat2=None, \
                         dTwilightLength2=None):
    """
    Plot the twilight length in minutes for all year.
    
    @type sFilename: string
    @param sFilename: Filename to save the graph. Filename is transformed like
     'file.png' to 'file_ss.png'.
    @type tToday: tuple
    @param tToday: Tuple as returned by the function time.gmtime.
    @type sBackgroundColor: string
    @param sBackgroundColor: Color that will be used for the background
    color for the generated graphics. See U{Chart of R colors<http://research.stowers-institute.org/efg/R/Color/Chart/>}
    @type  fLat1: float
    @param fLat1: Latitude in decimal for the first place.
    @type dTwilightLength1: dictionnary
    @param dTwilightLength1:  dictionnary containing the date in ISO 8601 format and
    sunset speed for this first place.
    @type  fLat2: float
    @param fLat2: Latitude in decimal for the second place.
    @type dTwilightLength2: dictionnary
    @param dTwilightLength2:  dictionnary containing the date in ISO 8601 format and
    sunset speed for this second place.

     
    """
    lDateISO8601 = list(dTwilightLength1.keys())
    lDateISO8601.sort()

    # Put everything in order
    lLength1 = []
    for sDay in lDateISO8601:
        lLength1.append(dTwilightLength1[sDay])
    if dTwilightLength2 is not None:
        lLength2 = []
        for sDay in lDateISO8601:
            lLength2.append(dTwilightLength2[sDay])
    else:
        lLength2 = [0]
        
    init_time_values(lDateISO8601)
    
    # Cairo and filename stuff
    sFilenameTwilightLength = crepyscule_tools.\
                         remove_extension(sFilename) +\
                         '_tl.png'
    sTitle = _("Twilight length")
    package_cairo = importr("Cairo")
    grdevices.png(sFilenameTwilightLength, type='cairo-png', w = nPngWidth, \
          h = nPngHeight, bg=sBackgroundColor, pointsize=8)


    # Get the sunset length for all the year for the first latitude
    nYear = int(lDateISO8601[0][0:4])
    
    # Labeling on Y axis
    sYLabel = _("Twilight length (minute)") 
    fYMax = max(max(lLength1),max(lLength2))

   # Horizontal lines
    nVerticalIncrement = fYMax/10
    if fYMax > 50:
        nVerticalIncrement = 10
    else:
        nVerticalIncrement = 5

    # Plot the fluxes
    graphics.par(las=1)
    graphics.plot(rX_axis, lLength1, ylim=r_base.c(0, fYMax), xaxs='i', yaxs='r',\
           lty=0, type='l',\
           xlab='Date', ylab=sYLabel, main=sTitle, xaxt='n', yaxt='n')
    lUsr = graphics.par("usr")
    graphics.axis(1, at=rlMonth, labels=lMonthString)
    graphics.axis(2, at=numpy.arange(0, fYMax, nVerticalIncrement).tolist())
    ### Grid
    rXdomain = r_base.c(lUsr[0], lUsr[1])
    for i in numpy.arange(0, fYMax, nVerticalIncrement):
        rYdomain = r_base.c(i,i)
        graphics.lines(rXdomain, rYdomain, col='gray70') # Horizontal lines
    rYdomain = r_base.c(lUsr[2], lUsr[3])
    for rnMonth in rlMonth:
        rXdomain = r_base.c(rnMonth, rnMonth)
        graphics.lines(rXdomain, rYdomain, col='gray70') # Vertical lines
    graphics.lines( r_base.c(rlMonth[0], rlMonth[0]), rYdomain, col='black')

    graphics.lines(rX_axis, lLength1, col='red')
    if fLat2 != None:
        graphics.lines(rX_axis, lLength2, col='orange')
    else: # Draw the value for today
        fTodayValue = lLength1[tToday[7]-1]
        sLabel = str(round(fTodayValue,2))
        npLength1 = numpy.array(lLength1)
        fMiddleOfGraph = npLength1.mean()
        __draw_today(tToday, fTodayValue, sLabel, fMiddleOfGraph)
    
    grdevices.dev_off()

    print ('filename', sFilenameTwilightLength)

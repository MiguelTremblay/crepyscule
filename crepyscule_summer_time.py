#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2005,2008  Miguel Tremblay

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not see  <http://www.gnu.org/licenses/>.
############################################################################

"""
Adjust the sunset and sunrise hour depending on the timezone.
Time zone are based on the Olson database. 
              
Project home page is U{http://ptaff.ca/crepyscule/}

 - Name: crepyscule_summer_time.py
 - Author:      U{Miguel Tremblay<http://ptaff.ca/miguel/>}
 - Date:        January 15th  2005
        
"""          
               
import time
import calendar

import pytz
import numpy

dYearSummertime = {}

def get_summer_time_days(sOlson, nYear):
    """
    Use pytz capabilities to return the start and end of daylight,
    in julian day, for a given year.

    @type sOlson: string
    @param sOlson: Olson string. For a complete list, see http://ur1.ca/orzma
    @type nYear: int
    @param nYear: Year to retrieve the dates for this summer code. 
    @rtype: tuple
    @return: First and last julian day for this summer time code

    See: http://ur1.ca/orzfs
    """
    # Default value if there is no summer time.
    nDayStart = 366
    nDayEnd = 0

    if sOlson != "--" and sOlson != "":
        tz = pytz.timezone(sOlson)
        try:
            lTransitionTime = tz._utc_transition_times        
            for i in range(len(lTransitionTime)):
                date = lTransitionTime[i]
                if date.year == nYear :
                    tt = date.timetuple()
                    nDayStart = tt.tm_yday
                    date2 = lTransitionTime[i+1]
                    tt2 = date2.timetuple()
                    nDayEnd = tt2.tm_yday
                    break
        except AttributeError:
            print ("Timezone has no summer time:", sOlson)

        

    return (nDayStart, nDayEnd)


def __get_julian(sDate):
    """
    Return julian day corresponding to iso 8601 date
    format is YYYY-MM-DD

    @type sDate: string
    @param sDate:  ISO8601 date. Format is YYYY-MM-DD

    @rtype: int
    @return: Julian day
    """
    tTime = time.strptime(sDate, '%Y-%m-%d')
    # The indices 7 is the julian day in struct_time
    return tTime[7]


def adjust_summer_time(lSunrise, lSunset, sTimezone, nYear):
    """
    Adjust the hour of sunset and sunrise depending of the summer time

    @type lSunrise: list
    @param lSunrise: List of sunrise for the whole year.
    @type lSunset: list
    @param lSunset: List of sunset for the whole year.
    @type sTimezone: string
    @param sTimezone: Summer time two letters code
    @type nYear: int
    @param nYear: Year to retrieve the dates for this summer code. 
     
    @rtype: list
    @return: List of the adjusted value of sunrise and sunset:
     [lSunrise, lSunset]
    """
    (nStart, nEnd) = get_summer_time_days(sTimezone, nYear)

    # Transform in numpy, less painfull
    npSunrise = numpy.array(lSunrise)
    npSunset = numpy.array(lSunset)

    # Northern hemisphere
    if nStart < nEnd:
        npSunrise[nStart:nEnd] = npSunrise[nStart:nEnd]+ 1
        npSunset[nStart:nEnd] = npSunset[nStart:nEnd]+ 1
    # Southern hemisphere
    else:
        npSunrise[0:nEnd] = npSunrise[0:nEnd] + 1
        npSunrise[nStart:len(npSunrise)] = \
           npSunrise[nStart:len(npSunrise)] + 1
        npSunset[0:nEnd] = npSunset[0:nEnd] + 1
        npSunset[nStart:len(npSunset)] = \
           npSunset[nStart:len(npSunset)] + 1

    return [npSunrise.tolist(), npSunset.tolist(), (nStart, nEnd)]

def adjust_summer_time_one_day(tDay, fValue, sTimezone, fLat):
    """
    Adjust the hour of sunset and sunrise depending of the summer time
    for this particular day.

    @type tDay: tuple
    @param tDay: Tuple of this day in the format (nYear, nMonth, nDay)
    @type fValue: float
    @param fValue: sunrise or sunset value to adjust following summertime.
    
    """
    global dYearSummertime

    # Get julian day
    (nYear, nMonth, nDay) = tDay
    sDate = str(nYear) + '-' + str(nMonth) + '-' + str(nDay)
    nJulian = __get_julian(sDate)

    # Check if this value have been asked before
    dCurrent = {}
    if nYear in dYearSummertime.keys():
        dCurrent = dYearSummertime[nYear]

    if sTimezone in dCurrent.keys():
        (nStart, nEnd) = dCurrent[sTimezone]
    else:
        # Get start and end date for this code
        (nStart, nEnd) = get_summer_time_days(sTimezone, nYear)
        # Add value in the global dictionary
        dCurrent[sTimezone] = (nStart, nEnd)
        dYearSummertime[nYear] = dCurrent

    # Adjust if needed
    # In Northern hemisphere
    if fLat > 0:
        if nJulian >= nStart and nJulian < nEnd:
            fValue = fValue + 1
    # Southern hemisphere
    else:
        if nJulian >= nStart or  nJulian < nEnd:
            fValue = fValue + 1

    return fValue
        
    
if  __name__ == "__main__":
    test()
